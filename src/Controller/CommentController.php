<?php

namespace App\Controller;

use App\Entity\Comments;
use App\Entity\User;
use App\Form\CommentsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\CommentsRepository;

class CommentController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    // /**
    //  * @Route("/about", name="about_us")
    //  */
    // public function index(): Response
    // {
    //     return $this->render('about_us/index.html.twig');
    // }

     /**
     * @Route("/about", name="about_us")
     */
    public function addComment(Request $request, CommentsRepository $commentList): Response
    {
        
        $comment = new Comments;

        $form = $this->createForm(CommentsType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $comment->setCreatedAt(new \DateTime('now'));
            $comment->setUser($this->getUser());
            $comment->getContent();

            $this->entityManager->persist($comment);
            $this->entityManager->flush();

            $this->addFlash('success', 'Votre commentaire a bien été envoyé et est en cours de validation !');

            return $this->redirectToRoute('about_us');

        }

        $liste_commentaires = $commentList->findAll();

        return $this->render('about_us/index.html.twig', [
            'comment' => $comment,
            'liste_commentaires' => $liste_commentaires,
            'form' => $form->createView()
        ]);
    } 
}
