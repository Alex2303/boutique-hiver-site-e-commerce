<?php


namespace App\Classe;


use App\Entity\Category;

class Search {

    /**
     * @var integer
     */
    public $page=1;
    /**
     * @var string
     */
    public $string = '';

    /**
     * @var Category[]
     */
    public $categories = [];

    /**
     * @var null|integer
     */
    public $max;

    /**
     * @var null|integer
     */
    public $min;


}