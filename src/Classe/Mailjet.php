<?php 

namespace App\Classe;

use Mailjet\Client;
use Mailjet\Resources;

Class Mailjet
{
    private $api_key = 'c434d22270874daa9f308f9cb6460d1c';
    private $api_key_secret = 'd0af44bee5f232e813a15e714be6fe56';

    public function send($to_email, $to_name, $subject, $content)
    {
        $mj = new Client($this->api_key, $this->api_key_secret, true,['version' => 'v3.1']);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => "desousa.alexandre23@gmail.com",
                        'Name' => "Mailjet Pilot"
                    ],
                    'To' => [
                        [
                            'Email' => $to_email,
                            'Name' => $to_name
                        ]
                    ],
                    'TemplateId' => 2322100,
                    'TemplateLanguage' => true,
                    'Subject' => $subject,
                    "Variables" => [
                            "content" => $content,
                    ]
                ]
            ]
        ];
        $response = $mj->post(Resources::$Email, ['body' => $body]);
        $response->success();
    }
}