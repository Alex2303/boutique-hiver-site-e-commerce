/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';
import './styles/carousel.css';
import noUiSlider from 'nouislider';
import 'nouislider/distribute/nouislider.css';
import Filter from './modules/Filter';
import 'ol/ol.css';
import Map from 'ol/Map';
import OSM from 'ol/source/OSM';
import Overlay from 'ol/Overlay';
import TileLayer from 'ol/layer/Tile';
import View from 'ol/View';
import {fromLonLat} from 'ol/proj';
import {transform} from 'ol/proj.js';

new Filter(document.querySelector('.js-filter'));

// start the Stimulus application
const $ = require('jquery');
require('bootstrap');

/****** ANIMATION HAMBURGER ON CLICK ******/

window.addEventListener('load' , function(){

    var burger = document.getElementsByClassName('burger')[0];

    burger.addEventListener('click', menuOpen)

    function menuOpen()
    {
        burger.classList.toggle('active');
    }

    const slider = document.getElementById('price-slider');

    if(slider){
        const min = document.getElementById('min')
        const max = document.getElementById('max')
        const minValue= Math.floor(parseInt(slider.dataset.min,10)/10)*10;
        const maxValue= Math.ceil(parseInt(slider.dataset.max,10)/10)*10;
        const range = noUiSlider.create(slider, {
            start: [min.value || minValue, max.value || maxValue],
            connect: true,
            step: 10,
            range: {
                'min': minValue,
                'max': maxValue
            }
        })


        range.on('slide', function(values, handle){
            console.log(values, handle);
            if(handle===0){

                min.value=Math.round(values[0]);
            }

            if(handle===1){

                max.value=Math.round(values[1]);
            }
        })

    }

});


var layer = new TileLayer({
    source: new OSM(),
  });
  
  var map = new Map({
    layers: [layer],
    target: 'map',
    view: new View({
      center: transform([6.3226072, 44.6008723], 'EPSG:4326', 'EPSG:3857'),
      zoom: 8,
    }),
  });


var pos = fromLonLat([6.3226072, 44.6008723]);

console.log(pos);

// Les Alpes marker
var marker = new Overlay({
    position: pos,
    positioning: 'center-center',
    element: document.getElementById('marker'),
    stopEvent: false,
  });
  map.addOverlay(marker);

